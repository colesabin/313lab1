import java.util.Scanner;

public class lab1 {
	public static void main(String[] args){
	
		// Create a Scanner that reads system input
		
		// Loop over the scanner's input
		// For each line of the input, send it to isPalindrome()
		// If isPalindrome returns true, print "This is a Palindrome." 
		// Otherwise print "Not a Palindrome."
		
		// Close the Scanner		
		Scanner scan = new Scanner(System.in);
		String input;
		int numLines;
		// Find out how many lines we have to scan
		numLines = Integer.parseInt(scan.nextLine());
		// Look for than many lines 
		while (numLines > 0){
			input = scan.nextLine();
			if (isPalindrome(input)){
				System.out.println("This is a Palindrome.");
			} else {
				System.out.println("Not a Palindrome.");
			}
			numLines --;
		}
		scan.close();
	}
	
	public static boolean isPalindrome(String s){
	
		// Create a stack and a Queue of chars that represents the passed in string
		// Hint: While you loop through the given string, push the same char onto your stack
		//		 that you enqueue into your Queue. This way you can use dequeue to get 
		//       the string from left to right, but you pop the string from right to left
		
		// Compare your Queue and Stack to see if the input String was a Palindrome or not	
		int length = s.length();
		Stack<Character> stack = new Stack<Character>();
		Queue<Character> queue = new Queue<Character>();
		//Put each char into both the stack and queue
		for (int i = 0; i < length; i++) {
			char c = s.charAt(i);
			stack.push(c);
			queue.enqueue(c);
		}//Pop and Deque and see if the chars match
		for (int i = 0; i < length; i++) {
			char r = stack.pop().getData();
			char q = queue.dequeue().getData();
			if ( r!= q){
				return false; //Just quit if one of em doesn't match.
			}
		}
		return true;
	}
	
	public static boolean isPalindromeEC(String s){
	
		// Implement if you wish to do the extra credit.
		return true;	
	}
}
